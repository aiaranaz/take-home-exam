package com.homecredit.the;

import com.homecredit.the.dto.City;
import com.homecredit.the.dto.CityWrapper;
import com.homecredit.the.entity.WeatherLog;
import com.homecredit.the.service.WeatherService;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import com.homecredit.the.repo.WeatherLogRepository;

/**
 * @author Aris Aranaz
 * @email aris.aranaz@gmail.com
 */
@Path("/weather")
@Produces(MediaType.APPLICATION_JSON)
public class WeatherAPI {
    
    @Autowired
    WeatherService weatherService;
    
    @Autowired
    WeatherLogRepository logRepository;
    
    @GET
    public Response getAllCity() {
        List<City> list = weatherService.getCityList();
        for(City c: list) {
            
            WeatherLog log = new WeatherLog();
            log.setLocation(c.getLocation());
            log.setTemperature(c.getTemperature());
            log.setActualWeather(String.join(", ", c.getWeather()));
            logRepository.save(log);
                
            if(logRepository.countByLocation(c.getLocation()) > 5) {
                WeatherLog toDelete = logRepository.findFirstByLocationOrderByDtimeInsertedAsc(c.getLocation());
                logRepository.delete(toDelete);
            }
        }
        return Response.ok(list).build();
    }
}
