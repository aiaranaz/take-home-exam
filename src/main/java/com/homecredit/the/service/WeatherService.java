package com.homecredit.the.service;

import com.homecredit.the.config.AppConfig;
import com.homecredit.the.dto.City;
import com.homecredit.the.dto.CityWrapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author Aris Aranaz
 * @email aris.aranaz@gmail.com
 */
@Service
public class WeatherService {
    private final RestTemplate restTemplate;
    
    @Autowired
    AppConfig appConfig;
    
    public WeatherService(){
        this.restTemplate = new RestTemplate();
    }
    public List<City> getCityList(){
        UriComponents uriComponents = UriComponentsBuilder.
                fromHttpUrl(appConfig.getUri())
                .query("appid={appid}").query("id={location_id}").query("units={units}")
                .buildAndExpand(appConfig.getAppId(),appConfig.getLocation(),appConfig.getUnits());
        return restTemplate.getForObject(uriComponents.toString(), CityWrapper.class).getList();
    }
    
    
}
