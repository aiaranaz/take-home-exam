package com.homecredit.the.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Aris Aranaz
 * @email aris.aranaz@gmail.com
 */
    
@Entity
@Table(name = "WeatherLog")
public class WeatherLog implements Serializable {
    @Id
    @GeneratedValue
    private Long Id;
    
    private String responseId;
    private String location;
    private String actualWeather;
    private String temperature;
    private Date dtimeInserted;
    
    public WeatherLog() {
        this.dtimeInserted = new Date();
        this.responseId = UUID.randomUUID().toString();
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getActualWeather() {
        return actualWeather;
    }

    public void setActualWeather(String actualWeather) {
        this.actualWeather = actualWeather;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public Date getDtimeInserted() {
        return dtimeInserted;
    }
    
}
