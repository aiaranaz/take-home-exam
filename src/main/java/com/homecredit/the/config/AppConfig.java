package com.homecredit.the.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author Aris Aranaz
 * @email aris.aranaz@gmail.com
 */
@Configuration
public class AppConfig {
    
    @Value("${app.openweathermap.uri}")
    private String uri;
    
    @Value("${app.openweathermap.appid}")
    private String appId;
    
    @Value("${app.openweathermap.location}")
    private String location;
    
    @Value("${app.openweathermap.temperature.units}")
    private String units;

    public String getAppId() {
        return appId;
    }

    public String getLocation() {
        return location;
    }

    public String getUnits() {
        return units;
    }

    public String getUri() {
        return uri;
    }
    
}
