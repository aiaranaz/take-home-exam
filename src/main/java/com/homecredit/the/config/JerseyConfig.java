package com.homecredit.the.config;

import com.homecredit.the.WeatherAPI;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * @author Aris Aranaz
 * @email aris.aranaz@gmail.com
 */
@Component
public class JerseyConfig extends ResourceConfig{
    public JerseyConfig() {
        register(WeatherAPI.class);
    }
}
