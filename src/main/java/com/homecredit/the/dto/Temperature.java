package com.homecredit.the.dto;

/**
 * @author Aris Aranaz
 * @email aris.aranaz@gmail.com
 */
public class Temperature {
    private double temp;

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }
}
