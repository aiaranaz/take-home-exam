package com.homecredit.the.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Aris Aranaz
 * @email aris.aranaz@gmail.com
 */
public class City {
    
    private String name;
    private List<Weather> weather;
    private Temperature main;
    public String getLocation() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List getWeather() {
        List list = new ArrayList();
        Iterator<Weather> weatherList = this.weather.iterator();
        while(weatherList.hasNext()){
            list.add(weatherList.next().getMain());
        }
        return list;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public void setMain(Temperature main) {
        this.main = main;
    }
    
    public String getTemperature(){
        return Double.toString(main.getTemp());
    }

}
