package com.homecredit.the.dto;

import java.util.List;

/**
 * @author Aris Aranaz
 * @email aris.aranaz@gmail.com
 */
public class CityWrapper {

    private List<City> list;

    public List<City> getList() {
        return list;
    }

    public void setList(List<City> list) {
        this.list = list;
    }

}
