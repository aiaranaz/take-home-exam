package com.homecredit.the.dto;

/**
 * @author Aris Aranaz
 * @email aaranaz@yondu.com
 */
public class Weather {
    private String main;

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }
}
