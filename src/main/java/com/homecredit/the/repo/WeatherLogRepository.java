package com.homecredit.the.repo;

import com.homecredit.the.entity.WeatherLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Aris Aranaz
 * @email aris.aranaz@gmail.com
 */
@Repository
public interface WeatherLogRepository extends JpaRepository<WeatherLog, Long>{
    
    WeatherLog findFirstByLocationOrderByDtimeInsertedAsc(String location);
    
    long countByLocation(String location);
    
}
