create table WeatherLog (
    Id bigint(20) not null auto_increment,
    responseId varchar(255),
    location varchar(255),
    actualWeather varchar(255),
    temperature varchar(255),
    dtimeInserted timestamp,
    primary key (id)
);
