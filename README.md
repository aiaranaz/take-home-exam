# run application

$ mvn spring-boot:run

# api endpoint

http://localhost:8080/api/weather

# h2 cache database

http://localhost:8080/h2-console/